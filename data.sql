--
-- PostgreSQL database dump
--

-- Dumped from database version 11.6
-- Dumped by pg_dump version 12.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

-- Data for Name: User; Type: TABLE DATA; Schema: public; Owner: -

INSERT INTO public."User" VALUES ('0000000e-000d-000c-000b-00000000000a', 'michel dupond', 'm.dupond@gmail.com', '0606060606', 'mdupond', NULL, NULL);
INSERT INTO public."User" VALUES ('1111111e-111d-111c-111b-11111111111a', 'michel dupont', 'm.dupont@gmail.com', '0707070707', 'mdupont', NULL, NULL);
INSERT INTO public."User" VALUES ('2222222e-222d-222c-222b-22222222222a', 'test test', 'test@test.fr', '0000000000', 'test', NULL, NULL);

-- Data for Name: Post; Type: TABLE DATA; Schema: public; Owner: -

INSERT INTO public."Post" VALUES ('z1212121-a121-b121-c121-d12121212121', 'first post', '2019-12-05', NULL, '1111111e-111d-111c-111b-11111111111a');
INSERT INTO public."Post" VALUES ('z3232323-a323-b323-c323-d32323232323', 'second post', '2019-12-07', NULL, '0000000e-000d-000c-000b-00000000000a');
INSERT INTO public."Post" VALUES ('9rererer-0rer-1rer-2rer-3rererererer', 'third post', '2019-12-10', NULL, '2222222e-222d-222c-222b-22222222222a');
INSERT INTO public."Post" VALUES ('0pmpmpmp-2pmp-3pmp-4pmp-5pmpmpmpmpmp', 'fourth post', '2020-01-15', NULL, '2222222e-222d-222c-222b-22222222222a');
INSERT INTO public."Post" VALUES ('0pmpmpmp-3pmp-4pmp-5pmp-6pmpmpmpmpmp', 'fifth post', '2020-01-18', NULL, '0000000e-000d-000c-000b-00000000000a');
INSERT INTO public."Post" VALUES ('0pmpmpmp-2pmp-3pmp-4pmp-888yyyyyyyy8', 'FROM THE FUTURE', '2021-12-31', NULL, '0000000e-000d-000c-000b-00000000000a');
INSERT INTO public."Post" VALUES ('9rererer-0rer-1rer-2rer-3yuyuyuyuyuy', 'FROM THE PAST', '1970-01-01', NULL, '1111111e-111d-111c-111b-11111111111a');
INSERT INTO public."Post" VALUES ('z1212121-a121-b121-c121-dklklklklklk', 'Happy new year !', '2019-12-30', NULL, '0000000e-000d-000c-000b-00000000000a');

-- Data for Name: Image; Type: TABLE DATA; Schema: public; Owner: -

INSERT INTO public."Image" VALUES ('3pmpmpmp-9pop-9pop-9pop-9pmpmpmpmpmp', 'https://s3.fr-par.scw.cloud/download.thegame.fail/a3232323-b444-c555-d666-12re12re21er.png', '2020-01-15', NULL, 'z1212121-a121-b121-c121-dklklklklklk');
INSERT INTO public."Image" VALUES ('3impmpmp-9pup-9pup-9pup-9pmpmpmpmpmp',
'https://s3.fr-par.scw.cloud/download.thegame.fail/b3232323-b444-c555-d666-12re12re21er.jpg', '2020-01-15', NULL, 'z3232323-a323-b323-c323-d32323232323');
INSERT INTO public."Image" VALUES ('3jmpmpmp-9pup-9pup-9pup-9pmpmpmpmpmp',
'https://s3.fr-par.scw.cloud/download.thegame.fail/c3232323-b444-c555-d666-12re12re21er.png', '2020-01-15', NULL, '0pmpmpmp-2pmp-3pmp-4pmp-5pmpmpmpmpmp');
INSERT INTO public."Image" VALUES ('3pmpmpmp-9pup-9pup-9pup-9pmpmpmpmpmp',
'https://s3.fr-par.scw.cloud/download.thegame.fail/d3232323-b444-c555-d666-12re12re21er.jpeg', '2020-01-15', NULL, '0pmpmpmp-3pmp-4pmp-5pmp-6pmpmpmpmpmp');
INSERT INTO public."Image" VALUES ('3pmpmpmp-9pip-9pip-9pip-9pmpmpmpmpmp',
'https://s3.fr-par.scw.cloud/download.thegame.fail/e3232323-b444-c555-d666-12re12re21er.png', '2020-01-15', NULL, '0pmpmpmp-2pmp-3pmp-4pmp-888yyyyyyyy8');

-- Data for Name: Comment; Type: TABLE DATA; Schema: public; Owner: -

INSERT INTO public."Comment" VALUES ('x1212121-a121-k121-j121-d12121212123', '...', '2020-01-20', NULL, '0pmpmpmp-2pmp-3pmp-4pmp-5pmpmpmpmpmp');
INSERT INTO public."Comment" VALUES ('x1212121-a121-l121-h121-d12121212123', 'this is a nice comment', '2019-12-01', NULL, '9rererer-0rer-1rer-2rer-3yuyuyuyuyuy');
INSERT INTO public."Comment" VALUES ('x1212121-a121-m121-g121-d12121212123', 'this is a bad comment', '2019-12-12', NULL, '0pmpmpmp-2pmp-3pmp-4pmp-888yyyyyyyy8');
INSERT INTO public."Comment" VALUES ('x1212121-a121-n121-f121-d12121212123', 'FIRST LOL', '2020-01-02', NULL, 'z1212121-a121-b121-c121-d12121212121');

-- Data for Name: Emote; Type: TABLE DATA; Schema: public; Owner: -

INSERT INTO public."Emote" VALUES ('z1212121-a121-b121-c121-d12121212121', 'like', NULL);
INSERT INTO public."Emote" VALUES ('y1212121-a121-b121-c121-d12121212122', 'heart', NULL);
INSERT INTO public."Emote" VALUES ('x1212121-a121-b121-c121-d12121212123', 'thumbs_up', NULL);
INSERT INTO public."Emote" VALUES ('w1212121-a121-b121-c121-d12121212124', 'dislike', NULL);

-- Data for Name: Reaction; Type: TABLE DATA; Schema: public; Owner: -

INSERT INTO public."Reaction" VALUES ('2222222e-222d-222c-222b-22222222222a', '0pmpmpmp-3pmp-4pmp-5pmp-6pmpmpmpmpmp', 'z1212121-a121-b121-c121-d12121212121');
INSERT INTO public."Reaction" VALUES ('0000000e-000d-000c-000b-00000000000a', '9rererer-0rer-1rer-2rer-3rererererer', 'z1212121-a121-b121-c121-d12121212121');
INSERT INTO public."Reaction" VALUES ('2222222e-222d-222c-222b-22222222222a', 'z1212121-a121-b121-c121-d12121212121', 'x1212121-a121-b121-c121-d12121212123');
INSERT INTO public."Reaction" VALUES ('1111111e-111d-111c-111b-11111111111a', '9rererer-0rer-1rer-2rer-3rererererer', 'w1212121-a121-b121-c121-d12121212124');
INSERT INTO public."Reaction" VALUES ('2222222e-222d-222c-222b-22222222222a', '9rererer-0rer-1rer-2rer-3yuyuyuyuyuy', 'w1212121-a121-b121-c121-d12121212124');

-- Data for Name: RelationStatus; Type: TABLE DATA; Schema: public; Owner: -

INSERT INTO public."RelationStatus" VALUES ('6eeeeeee-7rrr-8ttt-9yyy-0uuuuuuuuuuu', 'Follow');
INSERT INTO public."RelationStatus" VALUES ('sssssss5-ddd4-fff3-ggg2-hhhhhhhhhhh1', 'Block');

-- Data for Name: Relationship; Type: TABLE DATA; Schema: public; Owner: -

INSERT INTO public."Relationship" VALUES ('0000000e-000d-000c-000b-00000000000a', '6eeeeeee-7rrr-8ttt-9yyy-0uuuuuuuuuuu', '2222222e-222d-222c-222b-22222222222a');
INSERT INTO public."Relationship" VALUES ('1111111e-111d-111c-111b-11111111111a', '6eeeeeee-7rrr-8ttt-9yyy-0uuuuuuuuuuu', '0000000e-000d-000c-000b-00000000000a');
INSERT INTO public."Relationship" VALUES ('2222222e-222d-222c-222b-22222222222a', '6eeeeeee-7rrr-8ttt-9yyy-0uuuuuuuuuuu', '0000000e-000d-000c-000b-00000000000a');
INSERT INTO public."Relationship" VALUES ('0000000e-000d-000c-000b-00000000000a', '6eeeeeee-7rrr-8ttt-9yyy-0uuuuuuuuuuu', '1111111e-111d-111c-111b-11111111111a');
INSERT INTO public."Relationship" VALUES ('2222222e-222d-222c-222b-22222222222a', 'sssssss5-ddd4-fff3-ggg2-hhhhhhhhhhh1', '1111111e-111d-111c-111b-11111111111a');

--
-- PostgreSQL database dump complete
--